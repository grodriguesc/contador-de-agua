import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(title: "Contador de copos d'água", home: Home());
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _quatidadeDeAgua = 0;
  int _metaDoDia = 0;
  var _infoText = "";

  final myController = TextEditingController();

  void _bebaAgua200() {
    setState(() {
      _quatidadeDeAgua += 200;
      _metaDoDia = int.parse(myController.text.toString());
      if (_quatidadeDeAgua >= _metaDoDia) {
        _infoText = "Parabéns você é hidratado!";
      } else if (_quatidadeDeAgua < _metaDoDia) {
        _infoText = "Faltam ${_metaDoDia - _quatidadeDeAgua}";
      }

      if (_quatidadeDeAgua >= 2 * _metaDoDia) {
        _infoText = "VOCÊ É MUITO HIDRATADO!!!";
      }
    });
  }

  void _bebaAgua500() {
    setState(() {
      _quatidadeDeAgua += 500;
      _metaDoDia = int.parse(myController.text.toString());
      if (_quatidadeDeAgua >= _metaDoDia) {
        _infoText = "Parabéns você é hidratado!";
      } else if (_quatidadeDeAgua < _metaDoDia) {
        _infoText = "Faltam ${_metaDoDia - _quatidadeDeAgua}";
      }

      if (_quatidadeDeAgua >= 2 * _metaDoDia) {
        _infoText = "VOCÊ É MUITO HIDRATADO!!!";
      }
    });
  }

  void _bebaAgua1000() {
    setState(() {
      _quatidadeDeAgua += 1000;
      _metaDoDia = int.parse(myController.text.toString());
      if (_quatidadeDeAgua >= _metaDoDia) {
        _infoText = "Parabéns você é hidratado!";
      } else if (_quatidadeDeAgua < _metaDoDia) {
        _infoText = "Faltam ${_metaDoDia - _quatidadeDeAgua}";
      }

      if (_quatidadeDeAgua >= 2 * _metaDoDia) {
        _infoText = "VOCÊ É MUITO HIDRATADO!!!";
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Contador de água"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            heightFactor: 2,
            child: Text(
              "Eu tomei $_quatidadeDeAgua mls de água",
              style: TextStyle(fontSize: 25, color: Colors.lightBlue),
            ),
          ),
          Text(
            "Minha meta é: ",
            style: TextStyle(fontSize: 25, color: Colors.lightBlue),
          ),
          TextField(
            controller: myController,
            textAlign: TextAlign.center,
            decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'Quantos ml vai beber hoje?'),
          ),
          Text(
            _infoText,
            style: TextStyle(fontSize: 25, color: Colors.lightBlue),
          ),
          Padding(
              padding: EdgeInsets.symmetric(horizontal: 25),
              child: Row(
                children: [
                  Column(
                    children: [
                      FlatButton(
                        onPressed: () {
                          _bebaAgua200();
                        },
                        child: Image.asset(
                          "images/water-glass.png",
                          scale: 8,
                        ),
                      ),
                      Text(
                        "200ml",
                        style: TextStyle(fontSize: 16, color: Colors.lightBlue),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      FlatButton(
                        onPressed: () {
                          _bebaAgua500();
                        },
                        child: Image.asset(
                          "images/water-glass.png",
                          scale: 6,
                        ),
                      ),
                      Text(
                        "500ml",
                        style: TextStyle(fontSize: 16, color: Colors.lightBlue),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      FlatButton(
                        onPressed: () {
                          _bebaAgua1000();
                        },
                        child: Image.asset(
                          "images/water-glass.png",
                          scale: 4,
                        ),
                      ),
                      Text(
                        "1 l",
                        style: TextStyle(fontSize: 16, color: Colors.lightBlue),
                      ),
                    ],
                  ),
                ],
              )),
          Align(
            alignment: Alignment.center,
            child: RawMaterialButton(
              onPressed: () {
                setState(() {
                  _quatidadeDeAgua = 0;
                  _infoText = "Faltam ${_metaDoDia - _quatidadeDeAgua}";
                });
              },
              elevation: 2.0,
              fillColor: Colors.white,
              child: Icon(
                Icons.update,
                size: 35.0,
              ),
              padding: EdgeInsets.all(15.0),
              shape: CircleBorder(),
            ),
          )
        ],
      ),
    );
  }
}
